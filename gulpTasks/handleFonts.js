const gulp = require("gulp");
const del = require("del");
const ttf2woff = require("gulp-ttf2woff");
const ttf2woff2 = require("gulp-ttf2woff2");

module.exports = function () {
  gulp.task("ttf2woff", () => {
    return gulp
      .src(["app/fonts/*.ttf"])
      .pipe(ttf2woff({ clone: true }))
      .pipe(gulp.dest("build/fonts/"));
  });
};

module.exports = function () {
  gulp.task("ttf2woff2", () => {
    return gulp
      .src(["app/fonts/*.ttf"])
      .pipe(ttf2woff2())
      .pipe(gulp.dest("build/fonts/"));
  });
};

module.exports = async function (cb) {
  $.gulp.task("handleFonts", () => {
    del("build/fonts/**/*");

    return $.gulp.src("app/fonts/original/*").pipe($.gulp.dest("build/fonts/"));
    //   console.log("first");
    //   gulp
    //     .src(["app/fonts/*.ttf"])
    //     .pipe(ttf2woff({ clone: true }))
    //     .pipe(gulp.dest("build/fonts/"));

    //   console.log("second");
    //   return gulp
    //     .src(["app/fonts/*.ttf"])
    //     .pipe(ttf2woff2())
    //     .pipe(gulp.dest("build/fonts/"));
    //
  });
};
